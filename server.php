<?php

session_start();

class LoginUser {
    public function login($e, $p) {
        $newLogin = (object) [
            'email' => $e,
            'password' => $p
        ];

        if ($_SESSION['users'] === null) {
            $_SESSION['users'] = array();
        }

        array_push($_SESSION['users'], $newLogin);
    }

    public function clear() {
        session_destroy();
    }
}

$userLogin = new LoginUser();

if ($_POST['action'] === 'login') {
    $userLogin->login($_POST['email'], $_POST['password']);
} else if ($_POST['action'] === 'clear') {
    $userLogin->clear();
}

header('Location: ./index.php');

?>
